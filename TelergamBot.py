# -*- coding: utf-8 -*-
import telebot
import config
from telebot import types

#Token
bot = telebot.TeleBot(config.TOKEN)

#Вступительное сообщение
@bot.message_handler(commands=['start'])
def welcome(message):
	bot.send_message(message.chat.id, 'Приветсвую, <b>{0.first_name}</b>👋!\nЯ <b>{1.first_name}</b>🤖, бот созданный чтобы ответить на самые распространенные вопросы о <b>чипировании</b>.\nЧтобы увидеть вопросы на которые я могу ответить - нажмите на кнопку "<b>Вопросы</b>" на экранной клавиатуре👇'.format(message.from_user, bot.get_me()),parse_mode='html', reply_markup=markup)

#Экранная клавиатура
markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
item3 = types.KeyboardButton('Приветствие')
item1 = types.KeyboardButton('Вопросы')
item4 = types.KeyboardButton('Полезные ссылки')
item5 = types.KeyboardButton('Задайте свой вопрос')
item2 = types.KeyboardButton('Стоп')
markup.add(item3, item1, item4, item5, item2)

#Реакция бота на сообщения
@bot.message_handler(content_types=['text'])
def answers(message):
		if message.text == 'Вопросы':
			bot.send_message(message.chat.id, 'Я помогу тебе найти ответ на доступные мне вопросы!\nЧтобы получить ответ на итересующий тебя вопрос, нажми на нужную цифру на клавиатуре под этим сообщением👇 \n1.Что такое <b>чипирование</b>?\n2.Каковы <b>плюсы</b> чипирования?\n3.Каковы <b>минусы</b> чипировния?\n4.Каковы возможности <b>чипирования</b>?\n5.Как скоро <b>чипирование</b> может стать обыденностью?\n6.Для чего нужно <b>чипирование</b>?\n7.Это связано с <b>заговорами</b>?\n8.Чья это <b>идея</b>?\n9.Как происходит процесс <b>чипирования</b>?\n10.Безопасна ли <b>чипизация</b>?\n11.Кто ведущий <b>специалист</b> в данной теме на сегодня?\n12.Кто такой <b>Билл Гейтс</b>?'.format(message.from_user, bot.get_me()), parse_mode='html', reply_markup=answers)
		elif message.text == 'Приветствие':
			bot.send_message(message.chat.id, 'Приветсвую, <b>{0.first_name}</b>👋!\nЯ <b>{1.first_name}</b>🤖, бот созданный чтобы ответить на самые распространенные вопросы о <b>чипировании</b>.\nЧтобы увидеть вопросы на которые я могу ответить - нажмите на кнопку "<b>Вопросы</b>" на экранной клавиатуре👇'.format(message.from_user, bot.get_me()),parse_mode='html')	
		elif message.text == 'Стоп':
			bot.send_message(message.chat.id, 'Досвидание!👋\nЕсли вдруг вы не нашли ответ на интересующий вас вопрос, вы можете задать этот вопрос в нашей группе, а мы добавим ответ в бота!')
		elif message.text == 'Полезные ссылки':
			bot.send_message(message.chat.id, 'Здесь вы можете найти другую интересную информацию о чипировании', reply_markup=good)
		elif message.text == 'Задайте свой вопрос':
			bot.send_message(message.chat.id, 'Задайте свой вопрос в группе в <b>ВКонтакте</b>, а мы добавим ответ на него в бота\nhttps://vk.com/chipconsbot', parse_mode='html')
		else:
			bot.send_message(message.chat.id, 'Я не знаю что ответить 🤔\nДоступные команды есть на экранной клавиатуре 👇')
#Инлайновая клавиатура для вопросов			
answers = types.InlineKeyboardMarkup(row_width=2)
answ1 = types.InlineKeyboardButton('Ответ 1', callback_data='a1')
answ2 = types.InlineKeyboardButton('Ответ 2', callback_data='a2')
answ3 = types.InlineKeyboardButton('Ответ 3', callback_data='a3')
answ4 = types.InlineKeyboardButton('Ответ 4', callback_data='a4')
answ5 = types.InlineKeyboardButton('Ответ 5', callback_data='a5')
answ6 = types.InlineKeyboardButton('Ответ 6', callback_data='a6')
answ7 = types.InlineKeyboardButton('Ответ 7', callback_data='a7')
answ8 = types.InlineKeyboardButton('Ответ 8', callback_data='a8')
answ9 = types.InlineKeyboardButton('Ответ 9', callback_data='a9')
answ10 = types.InlineKeyboardButton('Ответ 10', callback_data='a10')
answ11 = types.InlineKeyboardButton('Ответ 11', callback_data='a11')
answ12 = types.InlineKeyboardButton('Ответ 12', callback_data='a12')
answers.add(answ1, answ2, answ3, answ4, answ5, answ6, answ7, answ8, answ9, answ10, answ11, answ12)

#Инлайновая клавиатура для ссылок
good = types.InlineKeyboardMarkup(row_width=2)
gd1 = types.InlineKeyboardButton('NEURALINK от Илона Маска', callback_data='gd1')
gd2 = types.InlineKeyboardButton('Ссылка 2', callback_data='gd2')
gd3 = types.InlineKeyboardButton('Ссылка 3', callback_data='gd3')
gd4 = types.InlineKeyboardButton('Ссылка 4', callback_data='gd4')
good.add(gd1, gd2, gd3, gd4)


#Ответы в инлайн клавиатуре
@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    try:
        if call.message:
            if call.data == 'a1':
                bot.send_message(call.message.chat.id, 'Ответ 1')
            if call.data == 'a2':
                bot.send_message(call.message.chat.id, 'Ответ 2')
            if call.data == 'a3':
            	bot.send_message(call.message.chat.id, 'Ответ 3')
            if call.data == 'a4':
            	bot.send_message(call.message.chat.id, 'Ответ 4')
            if call.data == 'a5':
            	bot.send_message(call.message.chat.id, 'Ответ 5')
            if call.data == 'a6':
            	bot.send_message(call.message.chat.id, 'Ответ 6')
            if call.data == 'a7':
            	bot.send_message(call.message.chat.id, 'Ответ 7')
            if call.data == 'a8':
            	bot.send_message(call.message.chat.id, 'Ответ 8')
            if call.data == 'a9':
            	bot.send_message(call.message.chat.id, 'Ответ 9')
            if call.data == 'a10':
            	bot.send_message(call.message.chat.id, 'Ответ 10')
            if call.data == 'a11':
            	bot.send_message(call.message.chat.id, 'Ответ 11')
            if call.data == 'a12':
            	bot.send_message(call.message.chat.id, 'Ответ 12')
            if call.data == 'gd1':
            	bot.send_message(call.message.chat.id, 'Презентация чипа <b>NEURALINK</b> от Илона Маска\nhttps://www.youtube.com/watch?v=8Y-1knDFTPc', parse_mode='html')
            if call.data == 'gd2':
            	bot.send_message(call.message.chat.id, 'Ссылка 2')
            if call.data == 'gd3':
            	bot.send_message(call.message.chat.id, 'Ссылка 3')
            if call.data == 'gd4':
            	bot.send_message(call.message.chat.id, 'Ссылка 4')					
            #убрать инлайн клавиатуру
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                reply_markup=None)

    except Exception as e:
        print(repr(e))
#Проверка новых сообщений                   
bot.polling(none_stop=True)
